package cz.cvut.fit.saloviho

import com.fasterxml.jackson.databind.SerializationFeature
import cz.cvut.fit.saloviho.controllers.ActorController
import cz.cvut.fit.saloviho.controllers.DirectorController
import cz.cvut.fit.saloviho.controllers.MovieController
import cz.cvut.fit.saloviho.dao.Actor
import cz.cvut.fit.saloviho.dao.Director
import cz.cvut.fit.saloviho.dao.Movie
import cz.cvut.fit.saloviho.dto.ActorDTO
import cz.cvut.fit.saloviho.dto.DirectorDTO
import cz.cvut.fit.saloviho.dto.MovieDTO
import cz.cvut.fit.saloviho.persistence.Actors
import cz.cvut.fit.saloviho.persistence.Directors
import cz.cvut.fit.saloviho.persistence.MovieActors
import cz.cvut.fit.saloviho.persistence.Movies
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.features.*
import io.ktor.http.*
import io.ktor.jackson.*
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.transactions.TransactionManager
import org.jetbrains.exposed.sql.transactions.transaction
import java.sql.Connection

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {

    /**
     * Using file database, because in-memory database in Exposed is destroyed between transactions
     * So Database.connect("jdbc:sqlite:file:test?mode=memory&cache=shared", "org.sqlite.JDBC") would not work
     * Same with H2
     * https://github.com/JetBrains/Exposed/issues/726
     */
    Database.connect("jdbc:sqlite:data.db", "org.sqlite.JDBC")
    TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE

    /**
     * Add controllers
     */
    val movieController = MovieController()
    val actorController = ActorController()
    val directorController = DirectorController()

    /**
     * Create all tables if not exist
     */
    transaction {
        SchemaUtils.create (Movies)
        SchemaUtils.create (MovieActors)
        SchemaUtils.create (Directors)
        SchemaUtils.create (Actors)
    }

    /**
     * Insert some data
     */
    transaction {
        val d1 = Director.new { name = "Anthony Russo" }
        val d2 = Director.new { name = "Cate Shortland" }
        val a1 = Actor.new { name = "Robert Downey Jr." }
        val a2 = Actor.new { name = "Scarlett Johansson" }
        val act1 = SizedCollection(listOf(a1, a2, Actor.new { name = "Florence Pugh" }, Actor.new { name = "Rachel Weisz"}))
        val act2 = SizedCollection(listOf(a1, a2, Actor.new { name = "Chris Evans"}, Actor.new { name = "Sebastian Stan" }))

        /**
         * Can't add actors through new expression, it leads to null pointer exception
         * Bug in Exposed (link was lost)
         */
        val m1 = Movie.new {
            title = "Black Widow"
            year = 2020
            poster = "https://m.media-amazon.com/images/M/MV5BNmFjNTA4MDAtMDM3ZS00NjFmLTg3YzQtM2ZkMzI0OTNmODcyXkEyXkFqcGdeQXVyODIyOTEyMzY@._V1_SX300.jpg"
            plot = "A film about Natasha Romanoff in her quests between the films Civil War and Infinity War."
            director = d2
        }

        m1.actors = act1

        val m2 = Movie.new {
            title = "Captain America: Civil War"
            year = 2016
            poster = "https://m.media-amazon.com/images/M/MV5BMjQ0MTgyNjAxMV5BMl5BanBnXkFtZTgwNjUzMDkyODE@._V1_SX300.jpg"
            plot = "With many people fearing the actions of super heroes, the government decides to push for the Hero Registration Act, a law that limits a hero's actions. This results in a division in The Avengers. Iron Man stands with this Act, claiming that their actions must be kept in check otherwise cities will continue to be destroyed, but Captain America feels that saving the world is daring enough and that they cannot rely on the government to protect the world. This escalates into an all-out war between Team Iron Man (Iron Man, Black Panther, Vision, Black Widow, War Machine, and Spider-Man) and Team Captain America (Captain America, Bucky Barnes, Falcon, Scarlet Witch, Hawkeye, and Ant Man) while a new villain emerges."
            director = d1
        }

        m2.actors = act2
    }

    TransactionManager.manager.defaultIsolationLevel = Connection.TRANSACTION_SERIALIZABLE
    install(CORS) {
        method(HttpMethod.Options)
        method(HttpMethod.Put)
        method(HttpMethod.Delete)
        method(HttpMethod.Patch)
        header(HttpHeaders.Authorization)
        header("MyCustomHeader")
        allowCredentials = true
        anyHost() // @TODO: Don't do this in production if possible. Try to limit it.
    }

    install(Authentication) {
    }

    install(ContentNegotiation) {
        jackson {
            enable(SerializationFeature.INDENT_OUTPUT)
        }
    }

    routing {
        route ("/api")                                              // Main api route
        {
            route("/movies") {                                      // Route for movie interactions
                get("/") {                                          // Get all movies
                    val response = movieController.getAll()
                    call.respond(response)
                }

                post("/") {                                         // Add new movie
                    val movieDTO = call.receiveOrNull<MovieDTO>()
                    val response = movieDTO?.let {
                        return@let movieController.create(movieDTO)
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                get("/{id}") {                                      // Get movie by id
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let movieController.getById(id) ?: HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                put("/{id}")                                        // Update movie info
                {
                    val id = call.parameters["id"]?.toIntOrNull()
                    val movieDTO = call.receiveOrNull<MovieDTO>()

                    if (id == null || movieDTO == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = id.let {
                            movieController.update(id, movieDTO) ?: HttpStatusCode.NotFound
                        }
                        call.respond(response)
                    }
                }

                delete("/{id}")                                     // Delete movie
                {
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let if (movieController.delete(id)) HttpStatusCode.OK else HttpStatusCode.BadRequest
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                get("/{id}/director") {                             // Get movie director
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let movieController.getDirector(id) ?: HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                put("/{id}/director/{director_id}")                 // Link director director_id to movie
                {
                    val id = call.parameters["id"]?.toIntOrNull()
                    val directorId = call.parameters["director_id"]?.toIntOrNull()

                    if (id == null || directorId == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = if (movieController.setDirector(
                                id,
                                directorId
                            )) HttpStatusCode.OK else HttpStatusCode.BadRequest
                        call.respond(response)
                    }
                }

                delete("/{id}/director")                            // Unlink director from movie
                {
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let if (movieController.removeDirector(id)) HttpStatusCode.OK else HttpStatusCode.BadRequest
                    } ?: HttpStatusCode.BadRequest

                    call.respond(response)
                }

                get("/{id}/actors") {                               // Get movie actors
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let movieController.getActors(id) ?: HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                put("/{id}/actors/{actor_id}") {                    // Link actor actor_id to movie
                    val id = call.parameters["id"]?.toIntOrNull()
                    val actorId = call.parameters["actor_id"]?.toIntOrNull()

                    if (id == null || actorId == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = if (movieController.addActor(
                                id,
                                actorId
                            )) HttpStatusCode.OK else HttpStatusCode.BadRequest
                        call.respond(response)
                    }
                }

                delete("/{id}/actors/{actor_id}") {                 // Unlink actor actor_id from movie
                    val id = call.parameters["id"]?.toIntOrNull()
                    val actorId = call.parameters["actor_id"]?.toIntOrNull()

                    if (id == null || actorId == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = if (movieController.removeActor(
                                id,
                                actorId
                            )
                        ) HttpStatusCode.OK else HttpStatusCode.BadRequest
                        call.respond(response)
                    }
                }
            }

            route("/actors") {                                      // Route for actor interactions
                get("/") {                                          // Get actors
                    val response = actorController.getAll()
                    call.respond(response)
                }

                post("/") {                                         // Add new actor
                    val actorDTO = call.receiveOrNull<ActorDTO>()
                    val response = actorDTO?.let {
                        return@let actorController.create(actorDTO)
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                get("/{id}") {                                      // Get actor by id
                    val id = call.parameters["id"]!!.toInt()
                    val response = actorController.getById(id) ?: HttpStatusCode.NotFound
                    call.respond(response)
                }

                put("/{id}") {                                      // Update actor info
                    val id = call.parameters["id"]?.toIntOrNull()
                    val actorDTO = call.receiveOrNull<ActorDTO>()

                    if (id == null || actorDTO == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = actorController.update(
                            id,
                            actorDTO
                        ) ?: HttpStatusCode.NotFound
                        call.respond(response)
                    }
                }

                delete("/{id}") {                                   // Delete actor
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let if (actorController.delete(id)) HttpStatusCode.OK else HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                get("/{id}/movies") {                               // Get actor movies
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let actorController.getMovies(id) ?: HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                put("/{id}/movies/{movie_id}") {                    // Link movie movie_id to actor
                    val id = call.parameters["id"]?.toIntOrNull()
                    val movieId = call.parameters["actor_id"]?.toIntOrNull()

                    if (id == null || movieId == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = if (actorController.addMovie(
                                id,
                                movieId
                            )) HttpStatusCode.OK else HttpStatusCode.BadRequest
                        call.respond(response)
                    }
                }

                delete("/{id}/movies/{movie_id}") {                 // Unlink movie movie_id from actor
                    val id = call.parameters["id"]?.toIntOrNull()
                    val movieId = call.parameters["actor_id"]?.toIntOrNull()

                    if (id == null || movieId == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = if (actorController.removeMovie(
                                id,
                                movieId
                            )
                        ) HttpStatusCode.OK else HttpStatusCode.BadRequest
                        call.respond(response)
                    }
                }
            }


            route("/directors") {                                   // Route for director interactions
                get("/") {                                          // Get directors
                    val response = directorController.getAll()
                    call.respond(response)
                }

                post("/") {                                         // Add new director
                    val directorDTO = call.receiveOrNull<DirectorDTO>()
                    val response = directorDTO?.let {
                        return@let directorController.create(directorDTO)
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                get("/{id}") {                                      // Get director by id
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let directorController.getById(id) ?: HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                put("/{id}") {                                      // Update director info
                    val id = call.parameters["id"]?.toIntOrNull()
                    val directorDTO = call.receiveOrNull<DirectorDTO>()

                    if (id == null || directorDTO == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = id.let {
                            return@let directorController.update(
                                id,
                                directorDTO
                            ) ?: HttpStatusCode.NotFound
                        }
                        call.respond(response)
                    }
                }

                delete("/{id}") {                                   // Delete director
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let if (directorController.delete(id)) HttpStatusCode.OK else HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                get("/{id}/movies") {                               // Get director movies
                    val id = call.parameters["id"]?.toIntOrNull()
                    val response = id?.let {
                        return@let directorController.getMovies(id) ?: HttpStatusCode.NotFound
                    } ?: HttpStatusCode.BadRequest
                    call.respond(response)
                }

                put("/{id}/movies/{movie_id}") {                    // Links movie movie_id to director
                    val id = call.parameters["id"]?.toIntOrNull()
                    val movieId = call.parameters["actor_id"]?.toIntOrNull()

                    if (id == null || movieId == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = if (directorController.addMovie(
                                id,
                                movieId
                            )
                        ) HttpStatusCode.OK else HttpStatusCode.BadRequest
                        call.respond(response)
                    }
                }

                delete("/{id}/movies/{movie_id}") {                 // Unlinks movie movie_id from director
                    val id = call.parameters["id"]?.toIntOrNull()
                    val movieId = call.parameters["actor_id"]?.toIntOrNull()

                    if (id == null || movieId == null)
                    {
                        call.respond(HttpStatusCode.BadRequest)
                    }
                    else
                    {
                        val response = if (directorController.removeMovie(
                                id,
                                movieId
                            )
                        ) HttpStatusCode.OK else HttpStatusCode.BadRequest
                        call.respond(response)
                    }
                }
            }
        }
    }
}

