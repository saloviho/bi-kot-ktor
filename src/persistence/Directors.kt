package cz.cvut.fit.saloviho.persistence

import org.jetbrains.exposed.dao.IntIdTable

object Directors : IntIdTable() {
    var name = varchar("name", 255)
}