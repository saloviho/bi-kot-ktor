package cz.cvut.fit.saloviho.persistence

import org.jetbrains.exposed.dao.IntIdTable

object Actors : IntIdTable() {
    var name = varchar("name", 255)
}