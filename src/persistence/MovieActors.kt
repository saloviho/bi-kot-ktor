package cz.cvut.fit.saloviho.persistence

import org.jetbrains.exposed.sql.Table

object MovieActors: Table() {
    var movie = reference("movie", Movies).primaryKey(0)
    var actor = reference("actor", Actors).primaryKey(1)
}