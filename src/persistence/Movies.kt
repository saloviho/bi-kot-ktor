package cz.cvut.fit.saloviho.persistence

import org.jetbrains.exposed.dao.IntIdTable

object Movies : IntIdTable() {
    var title = varchar("title", 255)
    var year = integer("year").nullable()
    var director = reference("director", Directors).nullable()
    var poster = varchar("poster", 255).nullable()
    var plot = text("plot").nullable()
}