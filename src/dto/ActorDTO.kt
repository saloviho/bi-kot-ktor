package cz.cvut.fit.saloviho.dto

data class ActorDTO(val id: Int, val name: String)