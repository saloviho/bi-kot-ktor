package cz.cvut.fit.saloviho.dto

data class DirectorDTO(val id: Int, val name: String)