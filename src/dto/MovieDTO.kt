package cz.cvut.fit.saloviho.dto

data class MovieDTO(val id: Int, val title: String, val year: Int?, val poster: String?, val plot: String?)