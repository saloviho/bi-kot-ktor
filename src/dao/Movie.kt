package cz.cvut.fit.saloviho.dao

import cz.cvut.fit.saloviho.dto.MovieDTO
import cz.cvut.fit.saloviho.persistence.MovieActors
import cz.cvut.fit.saloviho.persistence.Movies
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class Movie(id: EntityID<Int>) : IntEntity(id), Transferrable {
    companion object : IntEntityClass<Movie>(Movies)
    var title by Movies.title
    var year by Movies.year
    var director by Director optionalReferencedOn Movies.director
    var poster by Movies.poster
    var plot by Movies.plot
    var actors by Actor via MovieActors

    override fun toDTO() = MovieDTO(id.value, title, year, poster, plot)
}