package cz.cvut.fit.saloviho.dao

import cz.cvut.fit.saloviho.dto.ActorDTO
import cz.cvut.fit.saloviho.persistence.Actors
import cz.cvut.fit.saloviho.persistence.MovieActors
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class Actor(id: EntityID<Int>) : IntEntity(id), Transferrable {
    companion object : IntEntityClass<Actor>(Actors)
    var name by Actors.name
    var movies by Movie via MovieActors

    override fun toDTO() = ActorDTO(id.value, name)
}