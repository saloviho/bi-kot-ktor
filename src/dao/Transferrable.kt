package cz.cvut.fit.saloviho.dao

interface Transferrable {
    fun toDTO(): Any
}