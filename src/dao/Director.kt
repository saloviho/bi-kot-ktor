package cz.cvut.fit.saloviho.dao

import cz.cvut.fit.saloviho.dto.DirectorDTO
import cz.cvut.fit.saloviho.persistence.Directors
import cz.cvut.fit.saloviho.persistence.Movies
import org.jetbrains.exposed.dao.EntityID
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass

class Director(id: EntityID<Int>) : IntEntity(id), Transferrable {
    companion object : IntEntityClass<Director>(Directors)
    var name by Directors.name
    val movies by Movie optionalReferrersOn Movies.director

    override fun toDTO() = DirectorDTO(id.value, name)
}