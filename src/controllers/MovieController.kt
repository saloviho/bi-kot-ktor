package cz.cvut.fit.saloviho.controllers

import cz.cvut.fit.saloviho.dao.Actor
import cz.cvut.fit.saloviho.dao.Director
import cz.cvut.fit.saloviho.dao.Movie
import cz.cvut.fit.saloviho.dto.ActorDTO
import cz.cvut.fit.saloviho.dto.DirectorDTO
import cz.cvut.fit.saloviho.dto.MovieDTO
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.transactions.transaction

class MovieController
{
    /**
     * Inserts new Movie into database
     * @param m MovieDTO object with data, id field is ignored (auto increment is used)
     * @return created object
     */
    fun create(m: MovieDTO): MovieDTO? = transaction { Movie.new { title = m.title; year = m.year; plot = m.plot; poster = m.poster }.toDTO() }

    /**
     * Updates Movie object in database
     * @param id Movie id
     * @param m MovieDTO object with new data, id field is ignored (auto increment is used)
     * @return updated object
     */
    fun update(id: Int, m: MovieDTO): MovieDTO? = transaction {
        Movie.findById(id)?.also {
            it.title = m.title
            it.year = m.year
            it.plot = m.plot
            it.poster = m.poster
        }?.toDTO()
    }

    /**
     * Removes Movie object from database
     * @param id Movie id
     * @return true if success, false if fail
     */
    fun delete(id: Int) = transaction {
        Movie.findById(id)?.let { it.delete(); return@let true } ?: false
    }

    /**
     * Gets all Movies from database
     * @return Movie list
     */
    fun getAll() : List<MovieDTO> = transaction {
        Movie.all().map{ it.toDTO() }
    }

    /**
     * Gets Movie object from database by id
     * @param id Movie id
     * @return Movie object
     */
    fun getById(id: Int) : MovieDTO? = transaction {
        Movie.findById(id)?.toDTO()
    }

    /**
     * Gets Movie director from database by Movie id
     * @param id Movie id
     * @return Director object
     */
    fun getDirector(id: Int): DirectorDTO? = transaction {
        Movie.findById(id)?.director?.toDTO()
    }

    /**
     * Gets Movie actors from database by Movie id
     * @param id Movie id
     * @return Actors list
     */
    fun getActors(id: Int): List<ActorDTO>? = transaction {
        Movie.findById(id)?.actors?.map { it.toDTO() }
    }

    /**
     * Links Director with id director_id to Movie
     * @param id Movie id
     * @param director_id Director id
     * @return true if success, false if fail
     */
    fun setDirector(id: Int, director_id: Int) = transaction {
        Director.findById(director_id)?.let { Movie.findById(id)?.director = it; return@let true }
    } ?: false


    /**
     * Unlink movie director from Movie
     * @param id Movie id
     * @return true if success, false if fail
     */
    fun removeDirector(id: Int) = transaction {
        Movie.findById(id)?.let { it.director = null; return@let true }
    } ?: false

    /**
     * Links Actor with id actor_id to Movie
     * @param id Movie id
     * @param actor_id Actor id
     * @return true if success, false if fail
     */
    fun addActor(id: Int, actor_id: Int) = transaction {
        val actor = Actor.findById(actor_id)
        return@transaction if (actor == null) false
        else{
            Movie.findById(id)?.let {
                it.actors = SizedCollection(it.actors + actor)
                return@let true
            } ?: false
        }
    }

    /**
     * Unlinks Actor with id actor_id from Movie
     * @param id Movie id
     * @param actor_id Actor id
     * @return true if success, false if fail
     */
    fun removeActor(id: Int, actor_id: Int) = transaction {
        return@transaction Movie.findById(id)?.let {
            val actor = it.actors.find { it.id.value == actor_id }
            return@let if (actor == null) false
            else {
                it.actors = SizedCollection(it.actors - actor)
                true
            }
        } ?: false
    }
}