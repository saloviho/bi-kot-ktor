package cz.cvut.fit.saloviho.controllers

import cz.cvut.fit.saloviho.dao.Actor
import cz.cvut.fit.saloviho.dao.Movie
import cz.cvut.fit.saloviho.dto.ActorDTO
import cz.cvut.fit.saloviho.dto.MovieDTO
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.transactions.transaction

class ActorController
{
    /**
     * Inserts new Actor into database
     * @param a ActorDTO object with data, id field is ignored (auto increment is used)
     * @return created object
     */
    fun create(a: ActorDTO): ActorDTO? = transaction { Actor.new { name = a.name }.toDTO() }

    /**
     * Updates Actor object in database
     * @param id Director id
     * @param a ActorDTO object with new data, id field is ignored (auto increment is used)
     * @return updated object
     */
    fun update(id: Int, a: ActorDTO): ActorDTO? = transaction { Actor.findById(id)?.also { it.name = a.name }?.toDTO() }

    /**
     * Removes Actor object from database
     * @param id Actor id
     * @return true if success, false if fail
     */
    fun delete(id: Int) = transaction {
        Actor.findById(id)?.let { it.delete(); return@let true } ?: false
    }

    /**
     * Gets all Actors from database
     * @return Actor list
     */
    fun getAll() : List<ActorDTO> = transaction {
        Actor.all().map{ it.toDTO() }
    }

    /**
     * Gets Actor object from database by id
     * @param id Actor id
     * @return Actor object
     */
    fun getById(id: Int) : ActorDTO? = transaction {
        Actor.findById(id)?.toDTO()
    }

    /**
     * Gets Actor movies from database by Actor id
     * @param id Actor id
     * @return Movie list
     */
    fun getMovies(id: Int): List<MovieDTO>? = transaction {
        Actor.findById(id)?.movies?.map { it.toDTO() }
    }

    /**
     * Links Movie with id movie_id to Actor
     * @param id Actor id
     * @param movie_id Movie id
     * @return true if success, false if fail
     */
    fun addMovie(id: Int, movie_id: Int) = transaction {
        val movie = Movie.findById(movie_id)
        return@transaction if (movie == null) false
        else {
            Actor.findById(id)?.let {
                it.movies = SizedCollection(it.movies + movie)
                return@let true
            } ?: false
        }
    }

    /**
     * Unlinks Movie with id movie_id from Actor
     * @param id Actor id
     * @param movie_id Movie id
     * @return true if success, false if fail
     */
    fun removeMovie(id: Int, movie_id: Int) = transaction {
        return@transaction Actor.findById(id)?.let {
            val movie = it.movies.find { it.id.value == movie_id }
            return@let if (movie == null) false
            else {
                it.movies = SizedCollection(it.movies - movie)
                true
            }
        } ?: false
    }
}