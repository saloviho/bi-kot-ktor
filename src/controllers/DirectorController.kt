package cz.cvut.fit.saloviho.controllers

import cz.cvut.fit.saloviho.dao.Director
import cz.cvut.fit.saloviho.dao.Movie
import cz.cvut.fit.saloviho.dto.DirectorDTO
import cz.cvut.fit.saloviho.dto.MovieDTO
import org.jetbrains.exposed.sql.transactions.transaction

class DirectorController
{
    /**
     * Inserts new Director into database
     * @param d DirectorDTO object with data, id field is ignored (auto increment is used)
     * @return created object
     */
    fun create(d: DirectorDTO): DirectorDTO? = transaction { Director.new { name = d.name }.toDTO() }

    /**
     * Updates Director object in database
     * @param id Director id
     * @param d DirectorDTO object with new data, id field is ignored (auto increment is used)
     * @return updated object
     */
    fun update(id: Int, d: DirectorDTO): DirectorDTO? = transaction { Director.findById(id)?.also { it.name = d.name }?.toDTO() }

    /**
     * Removes Director object from database
     * @param id Director id
     * @return true if success, false if fail
     */
    fun delete(id: Int) = transaction {
        Director.findById(id)?.let { it.delete(); return@let true } ?: false
    }

    /**
     * Gets all Directors from database
     * @return Director list
     */
    fun getAll() : List<DirectorDTO> = transaction {
        Director.all().map{ it.toDTO() }
    }

    /**
     * Gets Director object from database by id
     * @param id Director id
     * @return Director object
     */
    fun getById(id: Int) : DirectorDTO? = transaction {
        Director.findById(id)?.toDTO()
    }

    /**
     * Gets Director movies from database by Director id
     * @param id Director id
     * @return Movie list
     */
    fun getMovies(id: Int): List<MovieDTO>? = transaction {
        Director.findById(id)?.movies?.map { it.toDTO() }
    }

    /**
     * Links Movie with id movie_id to Director
     * @param id Director id
     * @param movie_id Movie id
     * @return true if success, false if fail
     */
    fun addMovie(id: Int, movie_id: Int) = transaction {
        val movie = Movie.findById(movie_id)
        return@transaction if (movie == null) false
        else {
            Director.findById(id)?.let {
                movie.director = it
                return@let true
            } ?: false
        }
    }

    /**
     * Unlinks Movie with id movie_id from Director
     * @param id Director id
     * @param movie_id Movie id
     * @return true if success, false if fail
     */
    fun removeMovie(id: Int, movie_id: Int) = transaction {
        return@transaction Director.findById(id)?.let {
            val movie = it.movies.find { it.id.value == movie_id }
            return@let if (movie == null) false
            else {
                movie.director = null
                true
            }
        } ?: false
    }
}